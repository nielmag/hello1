# this is my first project
## Overview

I have made an fairly successful career out of being a people/process person.
  Though I will never be a technical/engineering coach, I felt like I should do
  Something to learn the technical side of agile.  I have observed
  very poor technical practices and can only currently speak theoretically
  without much practical experience to offer them.   I do this because as I
  Self proclaimed people person, I want to be able to speak the language of
  technical people to help add greater value to organizations.  

## Image

![my company logo](https://i.ibb.co/9qvSrkd/IN-VISION-LOGO-W-IMAGE-HOSTED.png)
