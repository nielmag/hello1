package hello.storytests;
import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith( Cucumber.class )
@CucumberOptions( features = "src/test/resources/storytests",
  plugin = {"pretty","html:target/cucumber","json:target/cucumber.json"})
public class JUnitDriverIT {}
