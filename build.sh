mvn clean install
#if [ -n "$SONAR_TOKEN" ]; then
#  mvn sonar:sonar -pl !features \
# -Dsonar.projectKey=DevOpsForAgileCoaches \
# -Dsonar.host.url=$SONAR_URL \
# -Dsonar.login=$SONAR_TOKEN
#fi
echo line 8
if [ `uname` = 'Linux' ]; then
  echo line 9
  mkdir -p scratch
  cp main/target/*.jar scratch
  sudo docker build -t "${IMAGE_NAME}" -f Dockerfile scratch
  if [ "$#" -eq 2 ]; then
    echo line 14
    sudo docker login -u $1 -p $2
    sudo docker push "${IMAGE_NAME}"
  fi
fi
